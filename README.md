# Oreo Board

The Neo Pixel Oreo Board

Named after the young Golden Hurricane who inspired it.

## Hardware

Schematic:

![](art/OreoSCH.jpg)

PCB:

![](art/OreoPCB.jpg)

## 2016

Every year is a new robot! Here are the [specifics for the 2016 robot](2016).

# T-Shirt Cannon

TODO