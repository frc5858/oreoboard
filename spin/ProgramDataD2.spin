pub getProgram
  return @zoeProgram

DAT
zoeProgram

'config
  byte 14
  byte 98
  byte 24
  byte 8

'eventInput
  byte 1, "INIT",0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0

'palette  ' 64 longs (64 colors)
  long 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  long 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  long 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  long 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

'patterns ' 16 pointers
  word 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

'callstack ' 32 pointers
  word 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  word 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

'variables ' Variable storage (2 bytes each)
  word 0,0,0,0,0,0,0,0

'pixbuffer ' 1 byte per pixel
  byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  byte 0,0

'events
  byte "INIT",0, $00,$3A
  byte "CLOCKWISE",0, $01,$3C
  byte "COUNTERCLOCKWISE",0, $01,$60
  byte "DRAWSLASH",0, $01,$84
  byte "PLOT",0, $01,$B1
  byte $FF

'INIT_handler
  '  configure (out=D2, length=98, hasWhite=false)
  '  var c
  '  var x
  '  var tmpx
  '  var tmpc
  '  var cnt
  '  var delay
  '  var tmp
  '  var masterc
  byte $09,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 '   defineColor(color=0,    W=0, R=0,   G=0,   B=0)    // Black 
  byte $09,$00,$01,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=1,    W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$02,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=2,    W=0, R=120, G=80,  B=4)    // Yellow(ish)    
  byte $09,$00,$03,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=3,    W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$04,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=4,    W=0, R=120, G=80,  B=4)    // Yellow(ish)  
  byte $09,$00,$05,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=5,    W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$06,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=6,    W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$07,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=7,    W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$08,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=8,    W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$09,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=9,    W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$0A,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=10,   W=0, R=120, G=80,  B=4)    // Yellow(ish)    
  byte $09,$00,$0B,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=11,   W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$0C,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=12,   W=0, R=120, G=80,  B=4)    // Yellow(ish)  
  byte $09,$00,$0D,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=13,   W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$0E,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=14,   W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$0F,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=15,   W=0, R=120, G=80,  B=4)    // Yellow(ish) 
  byte $09,$00,$10,$00,$00,$00,$50,$00,$78,$00,$04 '   defineColor(color=16,   W=0, R=120, G=80,  B=4)    // Yellow(ish)  
  byte $04,$05,$00,$0A '   [delay] = 10
  byte $04,$01,$00,$64 '   [x] = 100
  byte $04,$00,$00,$01 '   [c] = 1
  'TOP:
  byte $04,$07,$00,$02 '   [masterc] = 2
  'TOP1:
  byte $07,$00,$34 '   gosub(clockWise)
  byte $05,$07,$21,$80,$07,$00,$01 '   [masterc] = [masterc] - 1
  byte $06,$00,$03,$05,$80,$07,$00,$00 '   if([masterc]!=0)
  byte $03,$FF,$EB '     goto(top1)
  '__gotoFail_0:
  byte $0A,$00,$00 '   solid(color=0)
  byte $01,$01,$F4 '   pause(time=500)
  byte $0A,$00,$08 '   solid(color=8)
  byte $01,$03,$E8 '   pause(time=1000)
  byte $0A,$00,$00 '   solid(color=0)
  byte $01,$01,$F4 '   pause(time=500)
  byte $0A,$00,$08 '   solid(color=8)
  byte $01,$03,$E8 '   pause(time=1000)
  byte $0A,$00,$00 '   solid(color=0)
  byte $01,$01,$F4 '   pause(time=500)
  byte $03,$FF,$C6 '   goto(top)
  byte $08 ' }

'CLOCKWISE_handler
  byte $04,$01,$00,$64 '   [x] = 100
  'UP:
  byte $04,$04,$00,$08 '   [cnt] = 8
  byte $0A,$00,$00 '   solid(color=0)
  byte $07,$00,$3A '   gosub(drawSlash)
  byte $01,$80,$05 '   pause(time=[delay])
  byte $05,$01,$20,$80,$01,$00,$01 '   [x] = [x] + 1
  byte $06,$00,$03,$05,$80,$01,$00,$C6 '   if([x]!=198)
  byte $03,$FF,$E1 '     goto(up)
  '__gotoFail_0:
  byte $08 ' }

'COUNTERCLOCKWISE_handler
  byte $04,$01,$00,$64 '   [x] = 100
  'DOWN:
  byte $04,$04,$00,$08 '   [cnt] = 8
  byte $0A,$00,$00 '   solid(color=0)
  byte $07,$00,$16 '   gosub(drawSlash)
  byte $01,$80,$05 '   pause(time=[delay])
  byte $05,$01,$21,$80,$01,$00,$01 '   [x] = [x] - 1
  byte $06,$00,$03,$05,$80,$01,$00,$02 '   if([x]!=2)
  byte $03,$FF,$E1 '     goto(down)
  '__gotoFail_0:
  byte $08 ' }

'DRAWSLASH_handler
  byte $04,$02,$80,$01 '   [tmpx] = [x]
  byte $04,$03,$80,$00 '   [tmpc] = [c]
  'SLASHLOOP:
  byte $07,$00,$22 '   gosub(plot)
  byte $05,$02,$20,$80,$02,$00,$01 '   [tmpx] = [tmpx] + 1
  byte $05,$03,$20,$80,$03,$00,$01 '   [tmpc] = [tmpc] + 1
  byte $05,$04,$21,$80,$04,$00,$01 '   [cnt] = [cnt] - 1
  byte $06,$00,$03,$05,$80,$04,$00,$00 '   if([cnt]!=0)
  byte $03,$FF,$DD '     goto(slashLoop)
  '__gotoFail_0:
  byte $08 '   return
  byte $08 ' }

'PLOT_handler
  byte $04,$06,$80,$02 '   [tmp] = [tmpx]  
  byte $06,$00,$03,$0C,$80,$06,$00,$64 '   if ([tmp]<100) 
  byte $03,$00,$18 '     goto(plotLess)
  '__gotoFail_0:
  byte $06,$00,$03,$01,$80,$06,$00,$C5 '   if ([tmp]>197)
  byte $03,$00,$17 '     goto(plotMore)
  '__gotoFail_1:
  'PLOTIT:
  byte $05,$06,$21,$80,$06,$00,$64 '   [tmp] = [tmp] - 100
  byte $02,$80,$06,$80,$03 '   set(pixel=[tmp],color=[tmpc])
  byte $08 '   return
  'PLOTLESS:
  byte $05,$06,$20,$80,$06,$00,$62 '   [tmp] = [tmp] + 98
  byte $03,$FF,$E9 '   goto(plotIt)
  'PLOTMORE:
  byte $05,$06,$21,$80,$06,$00,$62 '   [tmp] = [tmp] - 98
  byte $03,$FF,$DF '   goto(plotIt)
  byte $08 ' }

